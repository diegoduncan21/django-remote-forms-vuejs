import json
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.middleware.csrf import CsrfViewMiddleware
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from django_remote_forms.forms import RemoteForm

from .forms import UserForm, AllFieldsForm


@csrf_exempt
def user_form(request):
    csrf_middleware = CsrfViewMiddleware()
    response_data = {}
    status_code = 400 # default

    if request.method == 'POST':
        request.POST = json.loads(request.body)
        # Process request for CSRF
        csrf_middleware.process_view(request, None, None, None)
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            status_code = 200
    else:
        # Get form definition
        form = UserForm(initial={'username': 'luis'})
        status_code = 200

    remote_form = RemoteForm(form)
    # Errors in response_data['non_field_errors'] and response_data['errors']
    response_data.update(remote_form.as_dict())

    response = HttpResponse(
        json.dumps(response_data, cls=DjangoJSONEncoder),
        content_type="application/json",
        status=status_code
    )

    # Process response for CSRF
    csrf_middleware.process_response(request, response)
    return response


@csrf_exempt
def all_fields_form(request):
    csrf_middleware = CsrfViewMiddleware()
    response_data = {}
    status_code = 400 # default

    if request.method == 'POST':
        request.POST = json.loads(request.body)
        # Process request for CSRF
        csrf_middleware.process_view(request, None, None, None)
        form = AllFieldsForm(request.POST)
        if form.is_valid():
            form.save()
            status_code = 200
    else:
        # Get form definition
        form = AllFieldsForm()
        status_code = 200

    remote_form = RemoteForm(form)
    # Errors in response_data['non_field_errors'] and response_data['errors']
    response_data.update(remote_form.as_dict())

    response = HttpResponse(
        json.dumps(response_data, cls=DjangoJSONEncoder),
        content_type="application/json",
        status=status_code
    )

    # Process response for CSRF
    csrf_middleware.process_response(request, response)
    return response