from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
	first_name = forms.CharField(required=True)
	email = forms.EmailField(required=True)

	class Meta:
		model = User
		fields = [
			"first_name",
			"last_name",
			"email",
			"username",
		]


class AllFieldsForm(forms.Form):
	boolean_field = forms.BooleanField(label="Es tal cosa?")
	char_field = forms.CharField()
	char_field_text_area = forms.CharField(widget=forms.Textarea)
	choice_field = forms.ChoiceField(choices=(
		("opcion_1", "Opcion 1"),
		("opcion_2", "Opcion 2"),
		("opcion_3", "Opcion 3"),
		("opcion_4", "Opcion 4"),
	))
	date_field = forms.DateField()
	date_time_field = forms.DateTimeField()
	decimal_field = forms.DecimalField()
	# duration_field = forms.DurationField()
	email_field = forms.EmailField()
	# file_field = forms.FileField()
	# file_path_field = forms.FilePathField(path='/')
	float_field = forms.FloatField()
	image_field = forms.ImageField()
	integer_field = forms.IntegerField()
	multiple_choice_field = forms.MultipleChoiceField(choices=(
		("opcion_1", "Opcion 1"),
		("opcion_2", "Opcion 2"),
		("opcion_3", "Opcion 3"),
		("opcion_4", "Opcion 4"),
	))
	null_boolean_field = forms.NullBooleanField()
	regex_field = forms.RegexField(regex='\[(.*?)\]')
	slug_field = forms.SlugField()
	time_field = forms.TimeField()
	url_field = forms.URLField()
	# uuid_field = forms.UUIDField()

	model_choice_field = forms.ModelChoiceField(queryset=User.objects.all())
	model_multiple_choice_field = forms.ModelMultipleChoiceField(queryset=User.objects.all())
