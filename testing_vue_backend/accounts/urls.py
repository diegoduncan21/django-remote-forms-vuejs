from django.conf.urls import url
import views

urlpatterns = [
    url(r'^user_form/', views.user_form),
    url(r'^all_fields_form/', views.all_fields_form),
]
