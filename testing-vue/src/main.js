import Vue from 'vue'
import App from './App'

import Materials from 'vue-materials'

Vue.use(Materials)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
